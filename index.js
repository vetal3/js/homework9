function elementList(arr, parent = document.body) {
    const ul = document.createElement("ul");
    parent.append(ul);
    arr.forEach((element) => {
        const li = document.createElement("li");
        ul.append(li);
        if (Array.isArray(element)) {
            elementList(element, li);
        } else {
            li.textContent = element;
        }
    });
}
elementList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);

let seconds = 3;
const countdown = setInterval(() => {
    console.log(seconds);
    seconds--;
    if (seconds === 0) {
        clearInterval(countdown);
        document.body.innerHTML = "";
    }
}, 1000);